package pl.sda.java20.project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.sda.java20.project.enums.Role;
import pl.sda.java20.project.services.UserDetailsServiceCustomImpl;

@Configuration
@EnableWebSecurity
//@Order(2)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceCustomImpl userDetailsServiceCustom;

    @Autowired
    public WebSecurityConfig(UserDetailsServiceCustomImpl userDetailsServiceCustom) {
        this.userDetailsServiceCustom = userDetailsServiceCustom;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").hasAnyAuthority(Role.USER.getRoleUser(), Role.ADMIN.getRoleUser())
                //na stronę "/" może dostać się tylko osoba zalogowana z rolą user lub admin
                .antMatchers("/register").permitAll()
                //wszyscy mogą wejść na stronę "/register"
                .antMatchers("/h2/**").permitAll()
                //wyłącznie na czas developmentu
                //.antMatchers("/users/**").hasAnyAuthority("ROLE_USER")
                //.antMatchers("/tasks/**").hasAnyAuthority("ROLE_USER")
                .antMatchers("/admin/**").hasAuthority(Role.ADMIN.getRoleUser())
                .and()
                //sklejanie łańcucha wywołań
                .csrf().disable()
                //na czas developmentu; csrf należy skonfigurować, żeby aplikacja działała
                .headers()
                //
                .frameOptions().disable()
                //dodane ze względu na h2; przeglądarki domyślnie blokują ramki, a h2 jest w ramce
                //.xssProtection().block(true).and()
                .and()
                .formLogin()
                //fomularz logowania dostarczony przez Spring
                .loginPage("/login")
                //dajemy znać springowi, pod jakim adresem znajduje sie formularz logowania
                .defaultSuccessUrl("/")
                //gdzie przekieruje po pomyślnym zalogowaniu, spring robi posta za nas, nie obsługujemy go w kontrolerze
                .permitAll();
                    //wszyscy mogą wejść na tę stronę
                //.defaultSuccessUrl("/tasks") //przekierowanie na kontroler
                //.failureUrl("/login?error=true")
                //.permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceCustom);
        //przyjmuje klasę, która dziedziczy po UserDetailsService
        //odpowiada za logikę autoryzacji w kontekście bazy danych
    }
}