package pl.sda.java20.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.java20.project.dto.ProjectForCreation;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.repos.ProjectRepo;
import pl.sda.java20.project.services.ProjectService;

import java.util.List;

@Controller
@RequestMapping("/projects")
public class ProjectController {
    private final ProjectRepo projectRepo;
    private final ProjectService projectService;


    public ProjectController(ProjectRepo projectRepo, ProjectService projectService) {
        this.projectRepo = projectRepo;
        this.projectService = projectService;
    }

    @GetMapping
    String getProject(Model model) {
        List<Project> projects = projectRepo.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

    @GetMapping("/create")
    String showProjectForm(Model model) {
        model.addAttribute("project", new ProjectForCreation());
        return "createProjectForm";
    }

    @PostMapping("/create")
    String createProject(@ModelAttribute("project") ProjectForCreation projectForCreation) {
        Project project = projectService.transformDTOintoProject(projectForCreation);
        projectRepo.save(project);
        return "redirect:/projects";
    }

}
