package pl.sda.java20.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.java20.project.dto.UserForRegistration;
import pl.sda.java20.project.exceptions.UserAlreadyExistsException;
import pl.sda.java20.project.repos.UserRepo;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserRepo userRepo;

    public RegisterController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping
    public String displayRegisterForm (Model model) {
        model.addAttribute("formData", new UserForRegistration());
        return "registerForm";
    }

    @PostMapping
    public String registerUser (@Valid @ModelAttribute("formData") UserForRegistration user,
                                BindingResult bindingResult,
                                Model model) throws UserAlreadyExistsException {
        if (!user.getPassword().equals(user.getRepeatedPassword())) {
            model.addAttribute("errorMessage", "[Passwords do not match]");
            bindingResult.addError(new ObjectError("formData",
                    "Passwords do not match"));
        }
        if (bindingResult.hasErrors()) {
            return "registerForm";
        }
        try {
            userRepo.registerUser(user.getName(), user.getEmail(), user.getPassword());
        } catch (UserAlreadyExistsException e) {
            System.out.println(e.getMessage());
            return "redirect:/register";
        }
        return "redirect:/users";
    }
}