package pl.sda.java20.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.java20.project.dto.TaskForCreation;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.repos.ProjectRepo;
import pl.sda.java20.project.repos.TaskRepo;
import pl.sda.java20.project.repos.UserRepo;
import pl.sda.java20.project.services.TaskService;

import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    private final TaskRepo taskRepo;
    private final UserRepo userRepo;
    private final ProjectRepo projectRepo;
    private final TaskService taskService;

    public TaskController(TaskRepo taskRepo, UserRepo userRepo, ProjectRepo projectRepo, TaskService taskService) {
        this.taskRepo = taskRepo;
        this.userRepo = userRepo;
        this.projectRepo = projectRepo;
        this.taskService = taskService;
    }

    @GetMapping
    String getTasks(Model model) {
        List<Task> taskList = taskRepo.findAll();
        model.addAttribute("taskList", taskList);
        return "tasks";
    }

    @GetMapping("/create")
    String showTaskForm(Model model) {
        model.addAttribute("task", new TaskForCreation());
        model.addAttribute("usersList", userRepo.findAll());
        model.addAttribute("projectsList", projectRepo.findAll());
        return "createTaskForm";
    }

    @PostMapping("/create")
    String createTask(@ModelAttribute("task") TaskForCreation taskForCreation) {
        Task task = taskService.transformDTOintoTask(taskForCreation);
        taskRepo.save(task);
        return "redirect:/tasks";
    }
}
