package pl.sda.java20.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.java20.project.dto.UserForCreation;
import pl.sda.java20.project.model.User;
import pl.sda.java20.project.repos.UserRepo;
import pl.sda.java20.project.services.UserDetailsServiceCustomImpl;

import java.util.List;

@Controller()
@RequestMapping("/users")
public class UserController {

    private final UserRepo userRepo;
    private final UserDetailsServiceCustomImpl userService;

    public UserController(UserRepo userRepo, UserDetailsServiceCustomImpl userService) {
        this.userRepo = userRepo;
        this.userService = userService;
    }

    @GetMapping
    String getUser(Model model) {
        List<User> userList = userRepo.findAll();
        model.addAttribute("userList", userList);
        return "users";
    }

    @GetMapping("/create")
    String registerUser(Model model) {
        model.addAttribute("user", new UserForCreation());
        return "createUserForm";
    }

    @PostMapping("/create")
    String createNote(@ModelAttribute("user") UserForCreation userForCreation) {
        User user = userService.transformDTOintoUser(userForCreation);
        userRepo.save(user);
        return "redirect:/users";
    }
}
