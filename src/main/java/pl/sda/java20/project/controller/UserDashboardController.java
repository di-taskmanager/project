package pl.sda.java20.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.services.ProjectService;

import java.util.List;

@Controller
@RequestMapping("/user/dashboard")
public class UserDashboardController {

    private final ProjectService projectService;

    @Autowired
    public UserDashboardController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    private String dashboard(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Project> projects = projectService.findProjectsAssignedToUser(username);
        model.addAttribute("projects", projects);
        return "/user/dashboard";
    }
}
