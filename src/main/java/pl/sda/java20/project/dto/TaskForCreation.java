package pl.sda.java20.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskForCreation {

    private String name;
    private String content;
    private String username;
    private String projectname;
}
