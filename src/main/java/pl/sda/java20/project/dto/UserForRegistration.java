package pl.sda.java20.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserForRegistration {

    @NotBlank(message = "Name must not be blank")
    private String name;

    @NotBlank(message = "Email must not be blank")
    @Pattern(regexp = "[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]+",
            message = "Email must match the following scheme: yourLogin@emailDomain.com")
    private String email;

    @NotBlank(message = "Password must not be blank")
    private String password;

    private String repeatedPassword;
}
