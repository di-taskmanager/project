package pl.sda.java20.project.enums;

import lombok.Getter;

public enum Role {
    ADMIN("ROLE_ADMIN"), USER("ROLE_USER");

    @Getter
    String roleUser;

    Role(String roleUser) {
        this.roleUser = roleUser;
    }

}
