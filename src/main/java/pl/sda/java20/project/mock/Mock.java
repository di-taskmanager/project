package pl.sda.java20.project.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sda.java20.project.enums.Role;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.model.User;
import pl.sda.java20.project.repos.ProjectRepo;
import pl.sda.java20.project.repos.TaskRepo;
import pl.sda.java20.project.repos.UserRepo;

import javax.annotation.PostConstruct;

@Component
public class Mock {
    private final UserRepo userRepo;
    private final ProjectRepo projectRepo;
    private PasswordEncoder passwordEncoder;
    private final TaskRepo taskRepo;

    @Autowired
    public Mock(UserRepo userRepo, PasswordEncoder passwordEncoder, ProjectRepo projectRepo, TaskRepo taskRepo) {
        this.userRepo = userRepo;
        this.projectRepo = projectRepo;
        this.passwordEncoder = passwordEncoder;
        this.taskRepo = taskRepo;
    }

    @PostConstruct
    void mock() {
        User user = User.builder()
                .email("user@user.pl")
                .name("user")
                .password(passwordEncoder.encode("user"))
                .role(Role.USER)
                .build();
        if (userRepo.findAll().stream().noneMatch(userToAdd -> userToAdd.getEmail().equals("user@user.pl"))) {
            userRepo.save(user);
        }
        User admin = User.builder()
                .email("admin@admin.pl")
                .name("admin")
                .password(passwordEncoder.encode("admin"))
                .role(Role.ADMIN)
                .build();
        if (userRepo.findAll().stream().noneMatch(userAdminToAdd -> userAdminToAdd.getEmail().equals("admin@admin.pl"))) {
            userRepo.save(admin);
        }
        Project project = Project.builder()
                .name("Demo project")
                .usersOnProject(userRepo.findAll())
                .projectTasks(taskRepo.findAll())
                .build();
        if (projectRepo.findAll().stream().noneMatch(projectToAdd -> projectToAdd.getName().equals("Demo project"))) {
            projectRepo.save(project);
        }
        Task task = Task.builder()
                .name("testTask")
                .content("Very important task")
                .user(user)
                .project(project)
                .build();
        if (taskRepo.findAll().stream().noneMatch(taskToAdd -> taskToAdd.getName().equals("testTask"))) {
            taskRepo.save(task);
        }

    }
}
