package pl.sda.java20.project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Project extends BasicEntity{

    @Column
    private String name;

    @OneToMany
    private List<Task> projectTasks;

    @ManyToMany
    private List<User> usersOnProject;
}
