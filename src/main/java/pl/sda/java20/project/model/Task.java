package pl.sda.java20.project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Task extends BasicEntity{

    @Column
    String name;

    @Column
    String content;

    @OneToOne
    @JoinTable(name = "task_project",
            joinColumns = {@JoinColumn(name = "task_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")})
    Project project;

    @ManyToOne
    User user;
}