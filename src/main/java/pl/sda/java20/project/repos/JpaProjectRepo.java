package pl.sda.java20.project.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.model.User;

import java.util.List;

public interface JpaProjectRepo extends JpaRepository<Project, String> {
    List<Project> findProjectByName(String name);


}
