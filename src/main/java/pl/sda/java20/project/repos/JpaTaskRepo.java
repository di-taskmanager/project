package pl.sda.java20.project.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.java20.project.model.Task;

public interface JpaTaskRepo extends JpaRepository<Task, String> {
    Task findTaskByName (String taskName);
}
