package pl.sda.java20.project.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.java20.project.model.User;

public interface JpaUserRepo extends JpaRepository<User, String> {
    User findUserByName(String name);

    User findUserByEmail(String email);

    boolean existsByEmail(String email);
}
