package pl.sda.java20.project.repos;

import org.springframework.stereotype.Repository;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProjectRepo implements ProjectRepoUtil {
    private final JpaProjectRepo jpaProjectRepo;

    public ProjectRepo(JpaProjectRepo jpaProjectRepo) {
        this.jpaProjectRepo = jpaProjectRepo;
    }

    public List<Project> findAll() {
        return jpaProjectRepo.findAll();
    }

    public List<Project> findByUser(String username) {
        List<Project> projects = jpaProjectRepo.findAll().stream()
                .filter(project -> project.getUsersOnProject().stream()
                        .anyMatch(user -> user.getName().equals(username))
                )
                .collect(Collectors.toList());
        return projects;
    }

    @Override
    public Project save(Project project) {
        return jpaProjectRepo.save(project);
    }

    @Override
    public Project registerProject(String name, List<Task> projectTasks, List<User> usersOnProject) {
        Project registeredProject = Project.builder()
                .name(name)
                .projectTasks(projectTasks)
                .usersOnProject(usersOnProject)
                .build();
        return jpaProjectRepo.save(registeredProject);
    }

    @Override
    public void deleteByName(String projectName) {
        jpaProjectRepo.deleteById(projectName);
    }

    @Override
    public void deleteAll() {
        jpaProjectRepo.deleteAll();
    }

    @Override
    public Project findByName(String projectName) {
        List<Project> projectList = jpaProjectRepo.findProjectByName(projectName);
        if (projectList != null) {
            return projectList.get(0);
        } else {
            System.out.println("User not found");
            return new Project().builder()
                    .name("")
                    .projectTasks(new ArrayList<>())
                    .usersOnProject(new ArrayList<>())
                    .build();
        }
    }
}
