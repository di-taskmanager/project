package pl.sda.java20.project.repos;

import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.model.User;

import java.util.List;

public interface ProjectRepoUtil {
    List<Project> findAll();

    Project save(Project project);

    Project registerProject
            (String name, List<Task> projectTasks, List<User> usersOnProject);

    void deleteByName(String projectName);

    void deleteAll();

    Project findByName(String projectName) throws Exception;
}
