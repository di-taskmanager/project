package pl.sda.java20.project.repos;

import org.springframework.stereotype.Repository;
import pl.sda.java20.project.model.Task;

import java.util.List;

@Repository
public class TaskRepo implements TaskRepoUtil {

    private final JpaTaskRepo jpaTaskRepo;

    public TaskRepo(JpaTaskRepo jpaTaskRepo) {
        this.jpaTaskRepo = jpaTaskRepo;
    }

    @Override
    public List<Task> findAll() {
        List<Task> allTasks = jpaTaskRepo.findAll();
        return allTasks;
    }

    @Override
    public Task save(Task task) {
        Task newTask = jpaTaskRepo.save(task);
        return newTask;
    }

    @Override
    public void deleteById(String id) {
        jpaTaskRepo.deleteById(id);
    }

    @Override
    public void deleteAll() {
        jpaTaskRepo.deleteAll();
    }

    @Override
    public Task findByName(String taskName) throws Exception {
        Task foundTask = jpaTaskRepo.findTaskByName(taskName);
        if (foundTask != null) {
            return foundTask;
        } else {
            throw new Exception("Task not found");
        }
    }
}
