package pl.sda.java20.project.repos;

import pl.sda.java20.project.model.Task;

import java.util.List;

public interface TaskRepoUtil {
    List<Task> findAll();

    Task save(Task task);

    void deleteById(String id);

    void deleteAll();

    Task findByName(String taskName) throws Exception;
}
