package pl.sda.java20.project.repos;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import pl.sda.java20.project.enums.Role;
import pl.sda.java20.project.exceptions.UserAlreadyExistsException;
import pl.sda.java20.project.model.User;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepo implements UserRepoUtil {

    private final JpaUserRepo jpaUserRepo;
    private final PasswordEncoder passwordEncoder;

    public UserRepo(JpaUserRepo jpaUserRepo, PasswordEncoder passwordEncoder) {
        this.jpaUserRepo = jpaUserRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = jpaUserRepo.findAll();
        return allUsers;
    }

    @Override
    public User save(User user) {
        User savedUser = jpaUserRepo.save(user);
        return savedUser;
    }

    @Override
    public void deleteById(String id) {
        jpaUserRepo.deleteById(id);
    }

    @Override
    public void deleteAll() {
        jpaUserRepo.deleteAll();
    }

    @Override
    public User findByName(String username) throws UsernameNotFoundException {
        User userByName = jpaUserRepo.findUserByName(username);
        if (userByName != null) {
            return userByName;
        } else {
            throw new UsernameNotFoundException("User not found");
//            return User.builder()
//                    .name("")
//                    .email("")
//                    .password("")
//                    .build();
        }
    }

    public User findByEmail(String email) throws UsernameNotFoundException {
        User userByEmail = jpaUserRepo.findUserByEmail(email);
        if (userByEmail != null) {
            return userByEmail;
        } else {
            throw new UsernameNotFoundException("User not found");
//            return User.builder()
//                    .name("")
//                    .email("")
//                    .password("")
//                    .build();
        }
    }

    @Override
    public User registerUser(String name, String email, String password) throws UserAlreadyExistsException {
        boolean userExists = jpaUserRepo.existsByEmail(email);
        if (userExists) {
            throw new UserAlreadyExistsException("User already exists");
        }
        User registeredUser = User.builder()
                .name(name)
                .email(email)
                .password(passwordEncoder.encode(password))
                .role(Role.USER)
                .build();
        return jpaUserRepo.save(registeredUser);
    }


    public List<String> loadUsernames() {
        return jpaUserRepo.findAll().stream()
                .map(User::getName)
                .collect(Collectors.toList());
    }
}
