package pl.sda.java20.project.repos;

import pl.sda.java20.project.dto.UserForRegistration;
import pl.sda.java20.project.exceptions.UserAlreadyExistsException;
import pl.sda.java20.project.model.User;

import java.util.List;

public interface UserRepoUtil {
    List<User> findAll();

    User save(User user);

    User registerUser (String name, String email, String password) throws UserAlreadyExistsException;

    void deleteById(String id);

    void deleteAll();

    User findByName(String username);
}
