package pl.sda.java20.project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.sda.java20.project.dto.ProjectForCreation;
import pl.sda.java20.project.model.Project;
import pl.sda.java20.project.repos.ProjectRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    private final ProjectRepo projectRepo;

    @Autowired
    public ProjectService(ProjectRepo projectRepo) {
        this.projectRepo = projectRepo;
    }

    public List<Project> findProjectsAssignedToUser(String username) {
        List<Project> projects = new ArrayList<>();
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        if (name != null && !name.equals("Anonymous")) {
            projects = projectRepo.findByUser(username);
        }
        return projects;
    }

    public Project transformDTOintoProject(ProjectForCreation projectForCreation) {
        Project project = Project.builder()
                .name(projectForCreation.getName())
                .projectTasks(new ArrayList<>())
                .usersOnProject(new ArrayList<>())
                .build();
        return project;
    }

}
