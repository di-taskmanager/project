package pl.sda.java20.project.services;

import org.springframework.stereotype.Service;
import pl.sda.java20.project.dto.TaskForCreation;
import pl.sda.java20.project.model.Task;
import pl.sda.java20.project.repos.ProjectRepo;
import pl.sda.java20.project.repos.UserRepo;

@Service
public class TaskService {
    private final UserRepo userRepo;
    private final ProjectRepo projectRepo;

    public TaskService(UserRepo userRepo, ProjectRepo projectRepo) {
        this.userRepo = userRepo;
        this.projectRepo = projectRepo;
    }

    public Task transformDTOintoTask(TaskForCreation taskForCreation) {
        Task task = Task.builder()
                .name(taskForCreation.getName())
                .content(taskForCreation.getContent())
                .user(userRepo.findByName(taskForCreation.getUsername()))
                .project(projectRepo.findByName(taskForCreation.getProjectname()))
                .build();
        return task;
    }
}
