package pl.sda.java20.project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.java20.project.dto.UserForCreation;
import pl.sda.java20.project.enums.Role;
import pl.sda.java20.project.model.User;
import pl.sda.java20.project.repos.UserRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceCustomImpl implements UserDetailsService {

    @Autowired
    private UserRepo userRepository;
    //    @Autowired
//    private RoleRepo roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        List authorities = new ArrayList<>();
//        user.getRole().stream().forEach(role -> {
        String roleUser = user.getRole().getRoleUser();
        SimpleGrantedAuthority simpleGrantedAuthority;
//        if (roleUser != null) {
        simpleGrantedAuthority = new SimpleGrantedAuthority(roleUser);
//        } else {
//            simpleGrantedAuthority = new SimpleGrantedAuthority("");
//        }
        authorities.add(simpleGrantedAuthority);
//        });
        org.springframework.security.core.userdetails.User springUser = new org.springframework.security.core.userdetails.User
                (user.getName(), user.getPassword(), authorities);
        //user springowy, potrzebujemy go, ponieważ posługujemy się formularzem dostarczanym przez Springa
        //przekazujemy name, password i listę ról
        return springUser;
    }

    public User transformDTOintoUser(UserForCreation userForCreation) {
        User user = User.builder()
                .name(userForCreation.getName())
                .email(userForCreation.getEmail())
                .password(passwordEncoder.encode("0000"))
                .role(Role.USER)
                .userTasks(new ArrayList<>())
                .projects(new ArrayList<>())
                .build();
        return user;
    }

}
